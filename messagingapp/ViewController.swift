//
//  ViewController.swift
//  messagingapp
//
//  Created by Seyit Kaya on 11.07.2019.
//  Copyright © 2019 Seyit Kaya. All rights reserved.
//


import UIKit
import Firebase
import UserNotifications
import FirebaseMessaging

class ViewController: UIViewController {

    
    @IBOutlet weak var textViewToken: UITextView!
    @IBOutlet weak var textViewToken2: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textViewToken.text = "Remote instance ID token  ==> \n  \n \(AppDelegate.getInstantID() ?? "")"
        textViewToken2.text = "Push token ==> \n \(AppDelegate.getPushToken() ?? "")"
    }


}

